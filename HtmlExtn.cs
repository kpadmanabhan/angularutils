﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AngularUtilities
{
    public static class HtmlExtn
    {
        private const string scriptTemplate = "<script type=\"text/ng-template\" id=\"{0}\">{1}</script>";

        public static IHtmlString EmbedViews(this HtmlHelper helper, params string[] paths)
        {
            if (paths == null)
                throw new ArgumentNullException("paths");

            var htmlScripts = new StringBuilder();

            foreach (var path in paths)
            {
                var directoryPath = GetPhysicalPath(path);

                if (Directory.Exists(directoryPath))
                {
                    var files = Directory.GetFiles(directoryPath, "*.html", SearchOption.AllDirectories);

                    htmlScripts.Append(GenerateScriptFromFiles(files, path));
                }
            }

            return new HtmlString(htmlScripts.ToString());
        }

        private static string GetPhysicalPath(string path)
        {
            return HttpContext.Current.Server.MapPath(path);
        }

        private static StringBuilder GenerateScriptFromFiles(IEnumerable<string> files, string appPath)
        {
            var fileContent = new StringBuilder();

            foreach (var filePath in files)
            {
                if (File.Exists(filePath))
                {
                    var scriptId = GetScriptId(filePath, appPath);

                    fileContent.AppendFormat(scriptTemplate, scriptId, File.ReadAllText(filePath));
                }
            }

            return fileContent;
        }

        private static string GetScriptId(string filePath, string appPath)
        {
            var backSlashedAppPath = appPath.Replace("/", "\\");

            // the magic number 1 is to ensure ~ is missed from the name
            var appFolderPath = backSlashedAppPath.Substring(1);

            var appPathStartIndex = filePath.IndexOf(appFolderPath, System.StringComparison.Ordinal);
            
            var backSlashedFileRelativePath = filePath.Substring(appPathStartIndex);

            return backSlashedFileRelativePath.Replace("\\", "/");
        }
    }
}