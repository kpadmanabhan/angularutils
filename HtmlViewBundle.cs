﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Optimization;
using Microsoft.Ajax.Utilities;

namespace AngularUtilities
{
    public class HtmlViewBundle : IBundleTransform
    {
        public void Process(BundleContext context, BundleResponse response)
        {
            var builder = new HtmlBundleBuilder();

            foreach (var assetFile in response.Files)
            {
                var virtualPath = assetFile.IncludedVirtualPath;

                var path = context.HttpContext.Server.MapPath(virtualPath);

                var fileContent = File.ReadAllText(path);

                builder.Register(assetFile.IncludedVirtualPath, fileContent);
            }

            var content = builder.ToString();

            if (context.EnableOptimizations)
            {
                var minifier = new Minifier();
                content = minifier.MinifyJavaScript(content);
            }

            response.Cacheability = HttpCacheability.Public;
            response.Content = content;
        }

        public class HtmlBundleBuilder
        {
            private readonly StringBuilder contentBuilder;

            private const string htmlScriptTemplate = "<script id=\"{0}\" type=\"text/ng-template\">{1}</script>";

            public HtmlBundleBuilder()
            {
                contentBuilder = new StringBuilder();
            }
            public void Register(string filePath, string fileContent)
            {
                contentBuilder.AppendFormat(htmlScriptTemplate, filePath, fileContent);
            }

            public override string ToString()
            {
                return contentBuilder.ToString();
            }
        }
    }
}
